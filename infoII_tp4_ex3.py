# Exercice 3 - Trying to learn GitLab - 28/1O/2024 - Vanesa
# This was and still is quite difficult for me (had to ask my friends for help sos)... 

import heapq

class File:
    def __init__(self):
        # So first it is needed to initialize an empty list to act as our priority queue : 
        self.queue = []

    def ajouter(self, priorite, donnee):
        # Then insert with negative priority to make it a max-priority queue :
        heapq.heappush(self.queue, (-priorite, donnee))

    def supprimer(self):
        # Now to remove the highest priority item :
        if self.queue:
            return heapq.heappop(self.queue)[1]
        else:
            return None

    def afficher(self):
        # Okay so now to display the queue in priority order for checking : 
        return [item[1] for item in sorted(self.queue, reverse=True)]

    def is_empty(self):
        return len(self.queue) == 0

    def size(self):
        return len(self.queue)
    
    # to test and see if it works : 
if __name__ == "__main__":
    file = File()
    file.ajouter(4, "Nathan")
    file.ajouter(2, "Julia")
    file.ajouter(1, "Amandine")
    file.ajouter(3, "Mathias")
    
    print("Queue after adding elements (in priority order):")
    print(file.afficher())  # Expected order of it : Nathan, Mathias, Julia, Amandine

    print("Removing elements based on priority:")
    print(file.supprimer())  # Should remove Nathan
    print(file.supprimer())  # Should remove Mathias
    print(file.supprimer())  # Should remove Julia
    print(file.supprimer())  # Should remove Amandine